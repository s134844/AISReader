package org.padawanconsulting.serial;

import org.apache.camel.spring.boot.FatJarRouter;
import org.apache.camel.spring.boot.FatWarInitializer;

/**
 * Created by Oliver on 01-12-2016.
 *
 * Class used for specifying the main class for the generated WAR file
 */
public class SerialFatWarInitializer extends FatWarInitializer {
    @Override
    protected Class<? extends FatJarRouter> routerClass() {
        return SerialFatJarRouter.class;
    }
}

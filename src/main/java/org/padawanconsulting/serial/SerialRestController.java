package org.padawanconsulting.serial;

import org.padawanconsulting.serial.types.SerialSetup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Created by Oliver on 01-12-2016.
 *
 * Defines a set of REST endpoints
 */
@RestController
@RequestMapping("/rest")
public class SerialRestController {

    @Autowired
    private SerialFatJarRouter serialFatJarRouter;

    // endpoint for configuration of the device
    @RequestMapping(path = "/setup", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity setupSerial(@RequestBody SerialSetup setup) {
        try {
            serialFatJarRouter.changeConfig(setup);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    // endpoint for shutting down the device
    @RequestMapping(path = "/shutdown", method = RequestMethod.GET)
    public ResponseEntity shutdown(@RequestParam(name = "areYouSure") String areYouSure) {
        if (areYouSure.equals("yes")) {
            try {
                serialFatJarRouter.getContext().stop();
                Runtime.getRuntime().exec("sudo shutdown -h now");
                return ResponseEntity.ok().build();
            } catch (IOException e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Device could not be shut down");
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            }
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

}

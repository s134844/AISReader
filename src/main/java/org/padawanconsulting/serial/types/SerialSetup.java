package org.padawanconsulting.serial.types;

/**
 * Created by Oliver on 02-12-2016.
 *
 * Object to hold a configuration for the serial connection and the REST client
 */
public class SerialSetup {

    // the baud rate
    private int baud;
    // number of data bits
    private int dataBits;
    // number of stop bits
    private int stopBits;
    // the rate that the device will send messages to web service
    private String sendRate;
    // the target URL to where the device should send messages
    private String target;
    // ID for database entry of the setup
    private String _id = "SerialSetup";
    // revision key for database entry
    private String _rev;
    // ID for being able to easier find an existing setup in the database
    public final static String ID = "SerialSetup";

    public int getBaud() {
        return baud;
    }

    public void setBaud(int baud) {
        this.baud = baud;
    }

    public int getDataBits() {
        return dataBits;
    }

    public void setDataBits(int dataBits) {
        this.dataBits = dataBits;
    }

    public String getSendRate() {
        return sendRate;
    }

    public void setSendRate(String sendRate) {
        this.sendRate = sendRate;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public int getStopBits() {
        return stopBits;
    }

    public void setStopBits(int stopBits) {
        this.stopBits = stopBits;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_rev() {
        return _rev;
    }

    public void set_rev(String _rev) {
        this._rev = _rev;
    }
}

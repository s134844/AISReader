package org.padawanconsulting.serial.types;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * Created by Oliver on 02-12-2016.
 */
public class MessageWithTimeStamp {

    // the AIS message as a byte array
    private byte[] message;
    // the time the message was received
    private String timeStamp;

    public MessageWithTimeStamp(byte[] message) {
        this.message = message;
        this.timeStamp = DateTime.now(DateTimeZone.UTC).toString();
    }

    public byte[] getMessage() {
        return message;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

}

package org.padawanconsulting.serial;

import org.apache.camel.spring.boot.FatJarRouter;
import org.lightcouch.CouchDbClient;
import org.padawanconsulting.serial.types.MessageWithTimeStamp;
import org.padawanconsulting.serial.types.SerialSetup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oliver on 01-12-2016.
 *
 * The main class of the project
 */
@SpringBootApplication
@ImportResource("context.xml")
public class SerialFatJarRouter extends FatJarRouter {

    // if the serial route has been configured
    private boolean initialized;

    // see bean definition in context.xml
    @Autowired
    private CouchDbClient couchDbClient;

    private SerialRoute serialRoute;


    public SerialFatJarRouter() throws Exception {
    }

    // called on startup
    @Override
    public void configure() throws Exception {
        // create a SerialRoute and add it to the camel context
        serialRoute = new SerialRoute(couchDbClient);
        getContext().addRoutes(serialRoute);

        // when termination signal has been sent to application call the shutDown() function of the serial route
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                serialRoute.shutDown();
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }));
    }

    // change the configuration of the device
    public void changeConfig(SerialSetup setup) throws Exception {
        // if a configuration already exists in the database update it, else save it as a new document
        if (couchDbClient.contains(setup.get_id())) {
            initialized = true;
            String rev = couchDbClient.find(SerialSetup.class, SerialSetup.ID).get_rev();
            setup.set_rev(rev);
            couchDbClient.update(setup);
        } else {
            couchDbClient.save(setup);
        }
        if (initialized) {
            // route must be removed and then added again to change the configuration
            List<MessageWithTimeStamp> buffer = serialRoute.stop();
            getContext().stopRoute(serialRoute.getID());
            boolean removed = getContext().removeRoute(serialRoute.getID());
            if (removed) {
                serialRoute = new SerialRoute(buffer, setup, couchDbClient);
                getContext().addRoutes(serialRoute);
            } else {
                log.error("Sender could not be reconfigured");
            }
        } else {
            serialRoute = new SerialRoute(new ArrayList<>(), setup, couchDbClient);
            getContext().addRoutes(serialRoute);
            initialized = true;
        }
    }
}
